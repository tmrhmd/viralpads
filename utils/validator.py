import re
import datetime


class Validator(object):
    @staticmethod
    def empty_field(field):
        if field in ("", None):
            return True
        return False

    @staticmethod
    def valid_email(email):
        if re.match('^[a-zA-Z0-9.!#$%&\'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$', email):
            return True
        return False

    @staticmethod
    def valid_password(password):
        return len(password) >= 6

    @staticmethod
    def confirm_password(password, confirmation):
        return password == confirmation

    @staticmethod
    def expired_reset_link(date):
        return date > (date + datetime.timedelta(hours=24))