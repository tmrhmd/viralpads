import tornado.web

from api.base import Base


class Dashboard(Base):
    @tornado.web.authenticated
    def get(self):
        self.render("routes/dashboard/index.html")


class Reports(Base):
    @tornado.web.authenticated
    def get(self):
        self.render("routes/reports/index.html")


class Billing(Base):
    @tornado.web.authenticated
    def get(self):
        self.render("routes/billing/index.html")


class Settings(Base):
    @tornado.web.authenticated
    def get(self):
        self.render("routes/settings/index.html")