import tornado.web

from api.base import Base


class Index(Base):
    @tornado.web.authenticated
    def get(self):
        self.render("routes/properties/index.html")


class Edit(Base):
    @tornado.web.authenticated
    def get(self):
        self.render("routes/properties/edit.html")