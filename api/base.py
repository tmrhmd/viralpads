import hashlib
import concurrent.futures
from json import loads

import tornado.gen
import tornado.web
import tornado.escape
from bson import ObjectId
from bson.json_util import dumps

from utils.validator import Validator


worker_pool = concurrent.futures.ThreadPoolExecutor(max_workers=4)


class Base(tornado.web.RequestHandler, Validator):
    @property
    def db(self):
        return self.application.db

    @property
    def mailer(self):
        return self.application.mailer

    def receive(self, chunk):
        return loads(chunk.decode("utf-8"))

    def respond(self, message):
        self.set_header("Content-Type", "application/json; charset=UTF-8")
        self.finish(dumps(message))

    def get_login_url(self):
        return "/user/signin"

    def get_current_user(self):
        user = self.get_secure_cookie("user")
        if not user:
            return None
        return self.db.users.find_one({"_id": ObjectId(user.decode("utf-8"))})

    def hash_password(self, password):
        return hashlib.sha512(
            self.application.settings["password_secret"].encode('utf-8') + password.encode('utf-8')).hexdigest()

    def get_password(self, password):
        p = worker_pool.submit(self.hash_password, password)
        return p.result()

    def set_flash(self, kind, message):
        message = tornado.escape.url_escape(str(message))
        self.set_cookie("flash_" + kind, message)

    def get_flash(self, name):
        message = self.get_cookie(name)
        self.clear_cookie(name)
        return tornado.escape.url_unescape(message)

    def user_exists(self, email):
        if self.db.users.find_one({"profile.email": email}):
            return True
        return False

    def write_error(self, status_code, **kwargs):
        self.render("pages/error.html", status_code=status_code)


class FlashModule(tornado.web.UIModule):
    def render(self):
        return self.render_string("modules/flash.html")