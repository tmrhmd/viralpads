$(function () {
    var signup = $('#signup_form');
    var signin = $('#signin_form');
    var forgot = $('#forgot_form');
    var reset = $('#reset_form');

    signup.form({
        first_name: {
            identifier: 'first_name',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter your first name'
                }
            ]
        },
        last_name: {
            identifier: 'last_name',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter your last name'
                }
            ]
        },
        phone: {
            identifier: 'phone',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter your phone number'
                }
            ]
        },
        email: {
            identifier: 'email',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter your email'
                },
                {
                    type: 'email',
                    prompt: 'Please enter a valid email'
                }

            ]
        },
        password: {
            identifier: 'password',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter a password'
                },
                {
                    type: 'length[6]',
                    prompt: 'Your password must be at least 6 characters'
                }
            ]
        }
    }, {
        revalidate: true,
        inline: true,
        on: 'blur'
    });

    signin.form({
        email: {
            identifier: 'email',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter your email'
                },
                {
                    type: 'email',
                    prompt: 'Please enter a valid email'
                }

            ]
        },
        password: {
            identifier: 'password',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter your password'
                }
            ]
        }
    }, {
        revalidate: true,
        inline: true,
        on: 'blur'
    });

    forgot.form({
        email: {
            identifier: 'email',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter your email'
                },
                {
                    type: 'email',
                    prompt: 'Please enter a valid email'
                }

            ]
        }
    }, {
        revalidate: true,
        inline: true,
        on: 'blur'
    });

    reset.form({
        password: {
            identifier: 'password',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Please enter a password'
                },
                {
                    type: 'length[6]',
                    prompt: 'The password must be at least 6 characters'
                }
            ]
        },
        password_confirmation: {
            identifier: 'password_confirmation',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Please confirm your password'
                },
                {
                    type: 'match[password]',
                    prompt: 'Confirmation doesn\'t match password'
                }
            ]
        }
    }, {
        revalidate: true,
        inline: true,
        on: 'blur'
    });
});