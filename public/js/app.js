$(function () {
    $('.ui.dropdown').dropdown();
    $('.ui.form input').popup({on: 'focus', position: 'bottom center'});

    $('.datepicker').pickadate({
        format: 'mm/dd/yyyy',
        formatSubmit: 'mm/dd/yyyy'
    });
});

//var adjectives, styles, mapOptions, map, geocoder, markersArray, addressInvalid, ra, rb, fa, ra2, rb2, streetNumber, streetName, neighborhood, cityName, subCityName, adminArea3, stateAbbrev, postalCode, addressLat, addressLong;
//styles = [{
//    "featureType": "water",
//    "stylers": [{"saturation": 43}, {"lightness": -11}, {"hue": "#0088ff"}]
//}, {
//    "featureType": "road",
//    "elementType": "geometry.fill",
//    "stylers": [{"hue": "#ff0000"}, {"saturation": -100}, {"lightness": 99}]
//}, {
//    "featureType": "road",
//    "elementType": "geometry.stroke",
//    "stylers": [{"color": "#808080"}, {"lightness": 54}]
//}, {
//    "featureType": "landscape.man_made",
//    "elementType": "geometry.fill",
//    "stylers": [{"color": "#ece2d9"}]
//}, {
//    "featureType": "poi.park",
//    "elementType": "geometry.fill",
//    "stylers": [{"color": "#ccdca1"}]
//}, {
//    "featureType": "road",
//    "elementType": "labels.text.fill",
//    "stylers": [{"color": "#767676"}]
//}, {
//    "featureType": "road",
//    "elementType": "labels.text.stroke",
//    "stylers": [{"color": "#ffffff"}]
//}, {"featureType": "poi", "stylers": [{"visibility": "off"}]}, {
//    "featureType": "landscape.natural",
//    "elementType": "geometry.fill",
//    "stylers": [{"visibility": "on"}, {"color": "#b8cb93"}]
//}, {"featureType": "poi.park", "stylers": [{"visibility": "on"}]}, {
//    "featureType": "poi.sports_complex",
//    "stylers": [{"visibility": "on"}]
//}, {"featureType": "poi.medical", "stylers": [{"visibility": "on"}]}, {
//    "featureType": "poi.business",
//    "stylers": [{"visibility": "simplified"}]
//}];
//mapOptions = {
//    center: {lat: 40.7127, lng: -74.0059},
//    zoom: 12,
//    styles: styles,
//    disableDefaultUI: true
//};
//map = new google.maps.Map(document.getElementById('mapCanvas'), mapOptions);
//geocoder = new google.maps.Geocoder();
//markersArray = [];
//addressInvalid = false;
//function placeMarkers() {
//    for (var i = 0; i < markersArray.length; i++) {
//        markersArray[i].setMap(map);
//        map.setCenter(markersArray[i].position);
//        map.setZoom(18);
//    }
//}
//
//function clearOverlays() {
//    for (var i = 0; i < markersArray.length; i++) {
//        markersArray[i].setMap(null);
//    }
//    markersArray.length = 0;
//}
//
//$scope.geocode = function () {
//    ra = jQuery('#rawAddress');
//    rb = jQuery('#rawBorough');
//    ra2 = $scope.property.rawAddress;
//    rb2 = $scope.property.rawBorough;
//    if (ra2 !== '' && rb2 !== '' && ra2 !== undefined && rb2 !== undefined) {
//        fa = ra2 + ", " + rb2 + ", NY";
//        geocoder.geocode({'address': fa}, function (results, status) {
//            if (status == google.maps.GeocoderStatus.OK) {
//                if (results[0]['partial_match'] === true) {
//                    ra.parent().parent().addClass("error");
//                    rb.parent().addClass("error");
//                    $scope.property.address = {};
//                    addressInvalid = true;
//                    return;
//                }
//                clearOverlays();
//                if (addressInvalid) {
//                    ra.parent().parent().removeClass("error");
//                    rb.parent().removeClass("error");
//                    addressInvalid = false;
//                }
//                var marker = new google.maps.Marker({
//                    position: results[0].geometry.location,
//                    map: null
//                });
//                markersArray.push(marker);
//                var addressComponents = results[0]["address_components"];
//                addressLat = results[0].geometry.location.lat();
//                addressLong = results[0].geometry.location.lng();
//                jQuery.each(addressComponents, function () {
//                    switch (this.types[0]) {
//                        case "street_number":
//                            streetNumber = this.short_name;
//                            break;
//                        case "route":
//                            streetName = this.short_name;
//                            break;
//                        case "neighborhood":
//                            neighborhood = this.short_name;
//                            break;
//                        case "locality":
//                            cityName = this.short_name;
//                            break;
//                        case "administrative_area_level_3":
//                            adminArea3 = this.short_name;
//                            break;
//                        case "sublocality_level_1": // Catches things like Brooklyn
//                            subCityName = this.short_name;
//                            break;
//                        case "administrative_area_level_1":
//                            stateAbbrev = this.short_name;
//                            break;
//                        case "postal_code":
//                            postalCode = this.short_name;
//                            break;
//                    }
//                });
//                if (cityName === '' && subCityName !== '') {
//                    cityName = subCityName;
//                }
//                if (cityName === '' && adminArea3 !== '') {
//                    cityName = adminArea3;
//                }
//                if (subCityName == 'Manhattan') {
//                    subCityName = 'New York';
//                }
//                $scope.property.address = {};
//                $scope.property.address = {
//                    "streetNumber": streetNumber,
//                    "street": streetName,
//                    "neighborhood": neighborhood,
//                    "city": subCityName,
//                    "state": stateAbbrev,
//                    "zip": postalCode,
//                    "formatted": streetNumber + ' ' + streetName + ', ' + subCityName + ', ' + stateAbbrev + ' ' + postalCode
//                };
//                $scope.property.rawAddress = streetNumber + ' ' + streetName;
//            } else {
//                ra.parent().parent().addClass("error");
//                rb.parent().addClass("error");
//                addressInvalid = true;
//            }
//            placeMarkers()
//        });
//    }
//    var beds, baths, adjective;
//    adjectives = ["adorable", "alluring", "beautiful", "brand-new", "breathtaking", "bright", "charming", "chic", "classic", "close-to-town", "cozy", "cute", "delightful", "distinguished", "dividable", "elegant", "exquisite", "extravagant", "featured", "huge", "immaculate", "large", "lovely", "luxurious", "magnificent", "majestic", "mint", "modern", "must-see", "new", "nice", "palatial", "panoramic", "peaceful", "picturesque", "prestigious", "prime", "pristine", "professionally decorated", "quaint", "quiet", "rare", "remarkable", "renovated",
//        "sophisticated", "spacious", "stunning", "sun-filled", "unique", "upscale", "well-lit", "well-loved"];
//    $scope.property = {
//        'type': 'apartment',
//        'description': {}
//    };
//    $scope.title = function () {
//        beds = $scope.property.details.bedrooms;
//        baths = $scope.property.details.bathrooms;
//        if (beds != '' && baths != '' && neighborhood != '' && beds != undefined && baths != undefined && neighborhood != undefined) {
//            if (beds == 0) beds = 'Studio';
//            adjective = adjectives[Math.floor(Math.random() * adjectives.length)];
//            adjective = adjective.charAt(0).toUpperCase() + adjective.slice(1);
//            if (beds == 'Studio') {
//                $scope.property.description.headline = adjective + ' ' + beds + ' / ' + baths + ' Bath in ' + neighborhood;
//            } else {
//                $scope.property.description.headline = adjective + ' ' + beds + ' Bed / ' + baths + ' Bath in ' + neighborhood;
//            }
//        }
//    };
//};