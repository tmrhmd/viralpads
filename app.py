# !/usr/bin/env python3

import os

import tornado.ioloop
import tornado.httpserver
import tornado.web

import api.base
import api.auth
import api.user
import api.properties

from utils.mailer import Mailer
from setup import setup_environment

from pymongo import MongoClient
from tornado.options import define, options, parse_command_line

define("port", default=3000, type=int)


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/user/signin/?", api.auth.Signin),
            (r"/user/signup/?", api.auth.Signup),
            (r"/user/signout/?", api.auth.Signout),
            (r"/user/forgot/?", api.auth.UserForgot),
            (r"/user/forgot/([0-9a-f]{32})/?", api.auth.UserForgotReset),
            (r"/dashboard", api.user.Dashboard),
            (r"/billing", api.user.Billing),
            (r"/settings", api.user.Settings),
            (r"/reports", api.user.Reports),
            (r"/properties", api.properties.Index),
            (r"/properties/new", api.properties.Edit),
            (r"/properties/([0-9a-f]{32})/edit", api.properties.Edit)
        ]
        settings = dict(
            app_name="ViralPads",
            static_path=os.path.join(os.path.dirname(__file__), "public"),
            template_path=os.path.join(os.path.dirname(__file__), "public", "html"),
            ui_modules=dict(FlashModule=api.base.FlashModule),
            mailgun_url=os.getenv("VIRALPADS_MAILGUN_URL"),
            mailgun_secret=os.getenv("VIRALPADS_MAILGUN_SECRET"),
            password_secret=os.getenv("VIRALPADS_PASSWORD_SECRET"),
            cookie_secret=os.getenv("VIRALPADS_COOKIE_SECRET"),
            login_url="/user/signin",
            debug=True
        )
        tornado.web.Application.__init__(self, handlers, **settings)

        if self.settings.get("debug"):
            self.dbc = MongoClient(os.getenv("VIRALPADS_DATABASE_URL_DEV"))
        else:
            self.dbc = MongoClient(os.getenv("VIRALPADS_DATABASE_URL"))
        self.db = self.dbc["viralpads-dev"]
        self.mailer = Mailer(self.settings["mailgun_secret"], self.settings["mailgun_url"])


def main():
    try:
        setup_environment()
        parse_command_line()
        http_server = tornado.httpserver.HTTPServer(Application())
        http_server.listen(options.port)
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        print("\nStopping application...\n")


if __name__ == "__main__":
    main()